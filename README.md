# README - Kaggle Marine Explorer - Whale Detection Challenge:

Coding style:
http://google-styleguide.googlecode.com/svn/trunk/google-r-style.html

Background:
========================================
Sound recording buoys in the North Atlantic detect specific calls from Right Whales. The Right Whale is threatened to become extinct and there are only around 400 left of them. When the call of a Right Whale is detected by a buoy, marine traffic in the area is warned so they can avoid the whale.

http://www.kaggle.com/c/whale-detection-challenge


HOWTO:
=======================================
Place wave files with source data in a source data folder. A csv file containing a human experts classification if there is a Right Whale call in the sound file is included for the training file. Run /bin/main2.R that trains the ais and generates csv files for the test set sound files. The best performing combination turned out a Cross-Validation weighted combination of a Support Vector Machine, a Random Forest and a Generalized Boosted Regression Model with an AUC of 0.935.
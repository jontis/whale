# dev rough
library(sound)
library(seewave)
library(e1071)

# read in wav
wav1 <- loadSample(filename='sourceData/wav/train1.wav')

# spectrum analyse wav
fft1 <- abs(fft(as.numeric(wav1$sound)))
fftShort <- e1071::stft(as.numeric(wav1$sound), coef=20)
fftShortExt <- seewave::stft.ext(file='sourceData/wav/train1.wav', wl=40)

# drop mirror side of spectrum
fft1 <- fft1[((length(fft1) / 2) + 1):length(fft1)]

# feature engineering